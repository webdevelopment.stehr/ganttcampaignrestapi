﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;
using AutoMapper;
using WebApplication1.DTOs;
using System.Web.Http.Cors;
using System.Diagnostics;

namespace WebApplication1.Controllers
{
    public class CampaignController : ApiController
    {
        private readonly CampaignAppDbContext _context;

        // check if start and end date are to close (1 hour min range for now)
        // should be moved to model validation (requires custom validation attributes)
        private bool DateSpannValid(DateTime start, DateTime end)
        {
            // checking if Start Date is before End Date, may be moved to Model validation?
            TimeSpan timeSpan = end - start;
            if (timeSpan.TotalHours <= 1)
                return false;
            else
                return true;
        }

        public CampaignController()
        {
            _context = new CampaignAppDbContext();
        }

        //get - api/Campaigns
        public IEnumerable<CampaignDTO> GetCampaigns()
        {
            return Mapper.Map<IEnumerable<Campaign>, IEnumerable<CampaignDTO>>(_context.Campaigns.ToList());
        }

        //get - api/Campaigns/30
        public IHttpActionResult GetCampaign(int id)
        {
            var campaign = _context.Campaigns.SingleOrDefault(c => c.Id == id);

            if(campaign == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            var campaignDTO = Mapper.Map<CampaignDTO>(campaign);

            return Ok(campaignDTO);
        }

        //auth

        //post - api/Campaign
        [HttpPost]
        public CampaignDTO CreateCampaign(CampaignDTO campaignDTO)
        {
            if(!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            if (!DateSpannValid(campaignDTO.StartDate, campaignDTO.EndDate))
            {
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Start date and end date spann should be greater than 1 hour."
                };

                throw new HttpResponseException(response);
            }

            var campaign = Mapper.Map<Campaign>(campaignDTO);

            _context.Campaigns.Add(campaign);
            _context.SaveChanges();

            //set newly created Id to DTO
            campaignDTO.Id = campaign.Id;

            return campaignDTO;
        }

        //put - api/Campaign/23
        [HttpPut]
        public IHttpActionResult UpdateCampaign(int id, CampaignDTO campaignDTO)
        {
            if(!ModelState.IsValid)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var campaignToUpdate = _context.Campaigns.SingleOrDefault(C => C.Id == id);

            if(campaignToUpdate == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (!DateSpannValid(campaignDTO.StartDate,campaignDTO.EndDate))
            {
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "Start date and end date spann should be greater than 1 hour."
                };

                throw new HttpResponseException(response);
            }

            Mapper.Map<CampaignDTO, Campaign>(campaignDTO, campaignToUpdate);

            _context.SaveChanges();

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteCampaign(int id)
        {
            var campaignToDelete = _context.Campaigns.SingleOrDefault(C => C.Id == id);

            if(campaignToDelete == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            _context.Campaigns.Remove(campaignToDelete);
            _context.SaveChanges();

            return Ok(id);
        }
    }
}
