namespace WebApplication1.Models
{
    using Newtonsoft.Json;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;
    using System.Linq;

    public class CampaignAppDbContext : DbContext
    {
        public CampaignAppDbContext()
            : base("name=Model11")
        {
        }
        
        public DbSet<Campaign> Campaigns { get; set; }

    }

    public class Campaign
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        public string Color { get; set; }
    }
}